#/bin/bash

DATA="${1:-SUA}"
ALPHA="${2:-0.4}"
echo "Vanilla MSTREAM on ${DATA} with alpha ${ALPHA}"
python3 "preprocess_${DATA}.py" --data "/data/${DATA}/${DATA}.csv" --config "/data/${DATA}/config_${DATA}.json"
mstream -t '/data/time.txt' -n '/data/numeric.txt' -c '/data/categ.txt' -o '/data/results/score.txt' -a ${ALPHA}
python3 results.py --label '/data/label.txt' --scores '/data/results/score.txt'
python3 describe.py

