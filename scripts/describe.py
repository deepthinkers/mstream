import pandas as pd

df = pd.read_csv("/data/results/results.csv")
ano = df["y_true"].sum()/df.shape[0]*100
print(f"anomaly % {ano}")
print(df["y_score"].describe(include="all"))