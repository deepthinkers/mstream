import pandas as pd
import argparse
import json
import os


def load_config(config_path: str):
    assert os.path.isfile(config_path)
    with open(config_path) as f:
        config = json.loads(f.read())
    return config


parser = argparse.ArgumentParser(description="Pre-process data")
parser.add_argument("--data", help="data file path", required=False, default="/data/UNSW/UNSW.csv")
parser.add_argument("--time", help="time file path", required=False, default="/data/time.txt")
parser.add_argument("--num", help="numeric file path", required=False, default="/data/numeric.txt")
parser.add_argument("--cat", help="cat file path", required=False, default="/data/categ.txt")
parser.add_argument("--label", help="label file path", required=False, default="/data/label.txt")
parser.add_argument("--config", help="config.json file path", required=False, default="/data/UNSW/config_UNSW.json")
args = parser.parse_args()

df = pd.read_csv(args.data)
print(f"data loaded of shape {df.shape}")

df["timeStamp"] = df["sequenceId"]
df.to_csv(args.time, columns=["timeStamp"], header=None, index=False)

config = load_config(args.config)
for i in config["categorical"]:
    df[i] = df[i].astype("category").cat.codes
df.to_csv(args.num, columns=config["numeric"], header=None, index=False)
df.to_csv(args.cat, columns=config["categorical"], header=None, index=False)

df.to_csv(args.label, columns=['Label'], header=None, index=False)

print("Finished creating")

