#### 1. How do we compute the feature hash of categorical and continuous variables?
   For categorical data, we use standard linear hash functions which simply takes an integer-value and 
   multiply it with hash key and take modulo b map integer-valued data randomly into b buckets, 
   i.e. (0,...,b-1), where b is a fixed number.
   For real-valued data, however, the paper finds randomized hash functions tend to lead to highly 
   uneven bucket distributions for certain input datasets. Instead, a streaming log-bucketization 
   approach is used. We first apply a log-transform to the data value, then perform 
   min-max normalization, where the min and max are maintained in a streaming manner and finally
   map it such that the range of feature values is evenly divided into b buckets, 
   i.e. (0,...,b-1).
#### 2. How do we compute the overall record hash?
   We first divide the entire record into two parts, one consisting of the categorical 
   features, and the other consisting of real-valued features. We then separately hash categorical
   features to get a bucket for the categorical features, and numerical features to get a bucket for 
   the numeric features. Finally, we take the sum modulo b of the bucket for categorical 
   and numeric features to get a bucket for the whole record. 
   We hash categorical and numeric features as follows:
   1. Categorical: We use standard linear hash functions to map each of the individual categorical
      features into b buckets, and then take sum modulo b to get one bucket index for all categorical
      features.
   2. Numerical: To compute the hash of a real-valued numeric features of dimension d, we choose 
      random vectors (a_1, a_2,..., a_k) each having dimensions d and independently sampled from a 
      Gaussian distribution with 0 mean where k is the log(b). We then compute the scalar product of 
      numeric features with each of these vectors. We then map the positive scalar products to 1 
      and the non-positive scalar products to 0 and then concatenate these mapped values to get a 
      k-bit string, then convert it from bitset into an integer between 0 and 2^k-1. This gives us the 
      integer for numeric features bucket between 0 and b.
#### 3. Is the time-tick used to calculate the expected count absolute or relative?
   The time-tick used in to compute score is the absolute value of the time-stamp of the 
   observation. We do see a change in scores when add a constant value to the time-stamp of all the 
   records.
#### 4. Is the feature score or record score bounded? What are the upper and lower bounds?
   No, the feature scores, and the record score are not bounded. The score always takes a non-positive
   value. The max value of score is achieved when the past count is 0, that is when the feature
   is a novelty and this gives us the score approximately equal to the log of the value of time-stamp, 
   So, the max value of score increases with increase in the value of time-stamp. 
#### 5. Does the algo process the streams one at a time or aggregate them over a time-tick?
   The algorithm processes each observation one at a time in an first in first out manner to give an
   anomaly score to the observation. But the feature counts of observations at a time-stamp, are 
   not discounted by the temporal decay factor alpha until an observation of next time-tick is being
   processed.
#### 6. How does a novel feature contribute towards an observation's score?
   The contribution of a novel feature to the total score of observation would be through individual
   feature score and that will be approximately equal to the log of the value of the time-stamp, and 
   also through record level score also approximately equal to the log of the value of the time-stamp.
#### 7. Does record score have the same value irrespective of how many $(>0)$ features are novel?
   Since, all the features are clubbed before bucketing the whole record, the record score 
   will depend only on the current count, and the past count of the bucket the record is in. So, if
   one novel feature buckets the record into a new bucket, the maximum possible record score at that given 
   time-stamp will be assigned to the record score.
#### 8. Is it possible to convert and interpret the scores as probability?
   Yes, we can normalize the feature scores by dividing them with the log of value of time-stamp, to get a 
   number between 0 and 1. Similarly, we can normalize the record score to get a probability of record
   being an anomaly. We can then multiply these probabilities to get a probability score for the 
   observation being an anomaly.
#### 9. How do we determine the threshold for an anomaly for each feature and total score?
   We can have different thresholds as follows:
   1. Have a running average of the recent past scores as a threshold.
   2. Given supervision on past observations, we can select the threshold giving the best f1 score.
   3. We can convert score to probability and then predict anomaly with that probability.
#### 10. Other than the CMS what other metrics are tracked? What are all the normalizations performed on the features?
   The metrics outside CMS that are being tracked are the minimum and the maximum value for all numeric
   features. The numeric features are min-max normalized before hashing them into buckets. 