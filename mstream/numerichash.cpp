#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

#include "numerichash.hpp"
#include <cmath>

using namespace std;
/*
Numerichash is a class with CMS named count and has insert, get_count, clear and lower methods.
*/
Numerichash::Numerichash(int r, int b) {
    num_rows = r;
    num_buckets = b;
    this->clear();
}

/*
The function takes the current normalized numeric feature and returns the bucket for the numeric feature.
It takes the normalized value between 0 and 1, multiplies it with b-1 and return the floor of the product as bucket.
*/
int Numerichash::hash(double cur_node) {
    int bucket;
    cur_node = cur_node * (num_buckets - 1);
    bucket = floor(cur_node);
    if(bucket < 0) 
        bucket = (bucket%num_buckets + num_buckets)%num_buckets;
    return bucket;
}

// This function inserts the current numerical value in the CMS count and increments the count with weight.
void Numerichash::insert(double cur_node, double weight) {
    int bucket;
    bucket = hash(cur_node);
    count[0][bucket] += weight;
}

// This function returns the count from first row of the CMS count of the numeric feature.
double Numerichash::get_count(double cur_node) {
    int bucket;
    bucket = hash(cur_node);
    return count[0][bucket];
}

// This function sets the CMS count values to 0.
void Numerichash::clear() {
    count = vector<vector<double> >(num_rows, vector<double>(num_buckets, 0.0));
}

// This function updates the values in the count CMS by multiplying it by a factor.
void Numerichash::lower(double factor) {
    for (int i = 0; i < num_rows; i++) {
        for (int j = 0; j < num_buckets; j++) {
            count[i][j] = count[i][j] * factor;
        }
    }
}