#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

#include <cstdlib>
#include "categhash.hpp"
#include <limits>

using namespace std;
/*
Categhash is a class with CMS named count and r number of linear hash functions. It also has insert, get_count, clear
and lower methods.
*/
Categhash::Categhash(int r, int b) {
    num_rows = r;
    num_buckets = b;
    hash_a.resize(num_rows);
    hash_b.resize(num_rows);
    for (int i = 0; i < num_rows; i++) {
        // a is in [1, p-1]; b is in [0, p-1]
        hash_a[i] = rand() % (num_buckets - 1) + 1;
        hash_b[i] = rand() % num_buckets;
    }
    this->clear();
}

/*
The function takes the current categorical feature and index i and returns the bucket in the ith row for the categorical
feature. It takes the categorical value and applies ith linear hash to return the bucket.
*/
int Categhash::hash(long a, int i) {
    int resid = (a * hash_a[i] + hash_b[i]) % num_buckets;
    return resid + (resid < 0 ? num_buckets : 0);
}

// This function inserts the current categorical value in the CMS count and increments the count with weight.
void Categhash::insert(long cur_int, double weight) {
    int bucket;
    for (int i = 0; i < num_rows; i++) {
        bucket = hash(cur_int, i);
        count[i][bucket] += weight;
    }
}

// This function returns the minimum of the counts from the r rows of CMS count of the categorical feature.
double Categhash::get_count(long cur_int) {
    double min_count = numeric_limits<double>::max();
    int bucket;
    for (int i = 0; i < num_rows; i++) {
        bucket = hash(cur_int, i);
        min_count = MIN(min_count, count[i][bucket]);
    }
    return min_count;
}

// This function sets the CMS count values to 0.
void Categhash::clear() {
    count = vector<vector<double> >(num_rows, vector<double>(num_buckets, 0.0));
}

// This function updates the values in the count CMS by multiplying it by a factor.
void Categhash::lower(double factor) {
    for (int i = 0; i < num_rows; i++) {
        for (int j = 0; j < num_buckets; j++) {
            count[i][j] = count[i][j] * factor;
        }
    }
}