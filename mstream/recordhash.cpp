#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

#include <cstdlib>
#include "recordhash.hpp"
#include <limits>
#include <bitset>
#include "lshhash.h"
#include <cmath>

using namespace std;
/*
The Recordhash is called in anom.cpp with number of rows and buckets, dimension of numeric and categorical features.
This function is for creating RecordHash has num_recordhash which is r number of collections of random vectors.
Each collection has k vectors of dimension dim1 drawn from normal distribution. Here k=log(b), where b is the number of
buckets for calculating numeric record hash. The RecordHash also has cat_recordhash which is a collection of r
integer vectors of dimension dim2, where each integer is between 0 and b. This collection of vectors num_recordhash and
cat_recordhash is used to compute the numeric hash and categorical hash for a given record.
*/
Recordhash::Recordhash(int r, int b, int dim1, int dim2) {
    num_rows = r;
    num_buckets = b;
    dimension1 = dim1;
    dimension2 = dim2;
    MTRand mtr;
    int log_bucket;

    num_recordhash.resize(num_rows);
    for (int i = 0; i < num_rows; i++) {
        log_bucket = ceil(log2(num_buckets));
        num_recordhash[i].resize(log_bucket);
        for (int j = 0; j < log_bucket; j++) {
            num_recordhash[i][j].resize(dimension1);
            for (int k = 0; k < dimension1; k++) {
                num_recordhash[i][j][k] = mtr.randNorm();
            }
        }
    }

    cat_recordhash.resize(num_rows);
    for (int i = 0; i < num_rows; i++) {
        cat_recordhash[i].resize(dimension2);
        for (int k = 0; k < dimension2 - 1; k++) {
            cat_recordhash[i][k] = (rand() % (num_buckets - 1) + 1);
        }
        // TODO figure this out
        if (dimension2)
            cat_recordhash[i][dimension2 - 1] = (rand() % num_buckets);
    }

    this->clear();
}

/*
numerichash function is called by the functions insert and get_count r number of times, where r is the number of rows.
This function computes the numeric hash for the current record by using the ith collection of vectors in num_recordhash.
The hash is calculated by taking dot product of numeric features of the record with the collection of k vectors to get
k values. These k values are mapped to 1 if positive and to 0 if non-positive. This gives us a k-bit string which is
then converted from bitset to an integer between 0 and b, as k=log(b).
*/
int Recordhash::numerichash(const vector<double> &cur_numeric, int i) {

    double sum = 0.0;
    int bitcounter = 0;
    int log_bucket = ceil(log2(num_buckets));
    bitset<30> b;

    for (int iter = 0; iter < log_bucket; iter++) {
        sum = 0;
        for (int k = 0; k < dimension1; k++) {
            sum = sum + num_recordhash[i][iter][k] * cur_numeric[k];
        }

        if (sum < 0)
            b.set(bitcounter, 0);
        else
            b.set(bitcounter, 1);
        bitcounter++;
    }

    return b.to_ulong();
}

/*
categhash function is called by the functions insert and get_count r number of times, where r is the number of rows.
This function computes the categorical hash for the current record by using the ith vector in cat_recordhash. The hash
is calculated by dot product of current categorical features and ith vector in cat_recordhash and taking modulo b, to
get a value between 0 and b.
*/
int Recordhash::categhash(vector<long> &cur_categ, int i) {

    int counter = 0;
    int resid = 0;

    for (int k = 0; k < dimension2; k++) {
        resid = (resid + cat_recordhash[i][counter] * cur_categ[counter]) % num_buckets;
        counter++;
    }
    return resid + (resid < 0 ? num_buckets : 0);
}

/*
insert function increments the record's r buckets by weight.
This function is takes numerical features, categorical features of current record and weight. It computes the hash of
the numeric features, the hash of the categorical features then sum the numeric and categorical hash modulo b to get
the bucket for the current record and then increase the count of that bucket by weight. This process is repeated for
the number of rows.
*/
void Recordhash::insert(vector<double> &cur_numeric, vector<long> &cur_categ, double weight) {
    int bucket1, bucket2, bucket;

    for (int i = 0; i < num_rows; i++) {
        bucket1 = numerichash(cur_numeric, i);
        bucket2 = categhash(cur_categ, i);
        bucket = (bucket1 + bucket2) % num_buckets;
        count[i][bucket] += weight;
    }
}

/*
get_count function returns the minimum count in the record's r buckets.
This function is takes numerical features and categorical features of current record It computes the hash of
the numeric features, the hash of the categorical features then sum the numeric and categorical hash modulo b to get
the bucket for the current record and then get the count in the bucket. This process is repeated for the number of rows.
The function returns the minimum of all the rows count.
*/
double Recordhash::get_count(vector<double> &cur_numeric, vector<long> &cur_categ) {
    double min_count = numeric_limits<double>::max();
    int bucket1, bucket2, bucket;
    for (int i = 0; i < num_rows; i++) {
        bucket1 = numerichash(cur_numeric, i);
        bucket2 = categhash(cur_categ, i);
        bucket = (bucket1 + bucket2) % num_buckets;
        min_count = MIN(min_count, count[i][bucket]);
    }
    return min_count;
}

/*
This function sets the count CMS values to 0.
*/
void Recordhash::clear() {
    count = vector<vector<double> >(num_rows, vector<double>(num_buckets, 0.0));
}

/*
This function updates the values in the count CMS by multiplying it by a factor.
*/
void Recordhash::lower(double factor) {
    for (int i = 0; i < num_rows; i++) {
        for (int j = 0; j < num_buckets; j++) {
            count[i][j] = count[i][j] * factor;
        }
    }
}