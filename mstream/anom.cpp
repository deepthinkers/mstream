#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

#include <vector>
#include <cmath>
#include <limits>
#include "anom.hpp"
#include "numerichash.hpp"
#include "recordhash.hpp"
#include "categhash.hpp"

/*
Function to compute the anomaly score given the total count tot, current count cur and current time cur_t. The anomaly
score depends on the squared difference between the current count and mean count divided by the mean.
Here the mean is the fraction tot/cur_t.
*/
double counts_to_anom(double tot, double cur, int cur_t) {
    double cur_mean = tot / cur_t;
	double sqerr = pow(MAX(0, cur - cur_mean), 2);
    return sqerr / cur_mean + sqerr / (cur_mean * MAX(1, cur_t - 1));
}
/*
This function is called in main.cpp and returns a vector with anomaly scores for the given numeric, categorical and
time data. Here the argument num_rows and num_buckets is the number of rows and buckets in the CMS with default value
2 and 1024 respectively assigned in main.cpp. The dimension1 and dimension2 represents the number of numeric and
categorical features respectively.
*/
vector<double> *mstream(vector<vector<double> > &numeric, vector<vector<long> > &categ, vector<int> &times, int num_rows,
                        int num_buckets, double factor, int dimension1, int dimension2) {

    int length = times.size(), cur_t = 1;

    Recordhash cur_count(num_rows, num_buckets, dimension1, dimension2);
    Recordhash total_count(num_rows, num_buckets, dimension1, dimension2);

    auto *anom_score = new vector<double>(length);

    vector<Numerichash> numeric_score(dimension1, Numerichash(num_rows, num_buckets));
    vector<Numerichash> numeric_total(dimension1, Numerichash(num_rows, num_buckets));
    vector<Categhash> categ_score(dimension2, Categhash(num_rows, num_buckets));
    vector<Categhash> categ_total(dimension2, Categhash(num_rows, num_buckets));

    vector<double> cur_numeric(0);
    vector<double> max_numeric(0);
    vector<double> min_numeric(0);
    if (dimension1) {
        max_numeric.resize(dimension1, numeric_limits<double>::min());
        min_numeric.resize(dimension1, numeric_limits<double>::max());
    }
    vector<long> cur_categ(0);

    /*
    This block iterates over each record one at a time. It lower the values of CMSs elements by factor if the current
    record timestamp is greater than the previous record timestamp.
    */
    for (int i = 0; i < length; i++) {
        if (i == 0 || times[i] > cur_t) {
            cur_count.lower(factor);
            for (int j = 0; j < dimension1; j++) {
                numeric_score[j].lower(factor);
            }
            for (int j = 0; j < dimension2; j++) {
                categ_score[j].lower(factor);
            }
            cur_t = times[i];
        }

        if (dimension1)
            cur_numeric.swap(numeric[i]);
        if (dimension2)
            cur_categ.swap(categ[i]);

        /*
        This block sum the anomaly scores from numeric features.It iterates over numeric features and maintains the
        mix max values for each numeric feature, normalize the numeric feature value as (value-min)/(max-min). Then
        passes the normalized value to be inserted. Then gets the current and total count for the numeric feature and
        calls the counts_to_anom function for getting the anomaly score for the numeric feature.
        */
        double sum = 0.0, t, cur_score;
        for (int node_iter = 0; node_iter < dimension1; node_iter++) {
            cur_numeric[node_iter] = log10(1 + cur_numeric[node_iter]);
            if (!i) {
                max_numeric[node_iter] = cur_numeric[node_iter];
                min_numeric[node_iter] = cur_numeric[node_iter];
                cur_numeric[node_iter] = 0;
            } else {
                min_numeric[node_iter] = MIN(min_numeric[node_iter], cur_numeric[node_iter]);
                max_numeric[node_iter] = MAX(max_numeric[node_iter], cur_numeric[node_iter]);
                if (max_numeric[node_iter] == min_numeric[node_iter]) cur_numeric[node_iter] = 0;
                else cur_numeric[node_iter] = (cur_numeric[node_iter] - min_numeric[node_iter]) /
                                 (max_numeric[node_iter] - min_numeric[node_iter]);
            }
            numeric_score[node_iter].insert(cur_numeric[node_iter], 1);
            numeric_total[node_iter].insert(cur_numeric[node_iter], 1);
            t = counts_to_anom(numeric_total[node_iter].get_count(cur_numeric[node_iter]),
                               numeric_score[node_iter].get_count(cur_numeric[node_iter]), cur_t);
            sum = sum+t;
        }
        cur_count.insert(cur_numeric, cur_categ, 1);
        total_count.insert(cur_numeric, cur_categ, 1);

        /*
        This block sum the anomaly scores from categorical features.It iterates over categorical features.
        It passes the categorical value to be inserted. Then gets the current and total count for the categorical
        feature and calls the counts_to_anom function for getting the anomaly score for the categorical feature.
        */
        for (int node_iter = 0; node_iter < dimension2; node_iter++) {
            categ_score[node_iter].insert(cur_categ[node_iter], 1);
            categ_total[node_iter].insert(cur_categ[node_iter], 1);
            t = counts_to_anom(categ_total[node_iter].get_count(cur_categ[node_iter]),
                               categ_score[node_iter].get_count(cur_categ[node_iter]), cur_t);
            sum = sum+t;
        }

        cur_score = counts_to_anom(total_count.get_count(cur_numeric, cur_categ),
                                   cur_count.get_count(cur_numeric, cur_categ), cur_t);
        /*
        Finally the anomaly score of total record is added to the sum of anomaly scores of numerical and categorical
        features and takes the log of sum+1.
        */
        sum = sum + cur_score;
        (*anom_score)[i] = log(1 + sum);

    }
    return anom_score;
}
