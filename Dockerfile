#Build the source
FROM ubuntu:20.04 as builder
RUN apt update && apt install build-essential make -y
WORKDIR /mstream
COPY ./mstream /mstream
RUN make clean && make

#Package the runner
FROM ubuntu:20.04 as runner
# TODO use a compatible python image
RUN apt update && apt install python3-pip -y
COPY ./requirements/requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY --from=builder /mstream/*.o /bin/
COPY --from=builder /mstream/mstream /bin/
WORKDIR /experiment



